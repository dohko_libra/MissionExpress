/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.missionexpress.utilidades;

/**
 *
 * @author dohko
 */
public class Constant {

    /**
     * punctuation marks
     */
    public static final String COMMA = ",";
    public static final String EMPTY = "";
    public static final String SCRIPT = "-";
    public static final String TWO_POINT = ":";

    
    /**
     * Numbers
     */
    public static final int ONE = 0;
    public static final int TWO = 1;
    public static final int THREE = 2;
    public static final int FOUR = 3;
    public static final int FIVE = 4;
    public static final int SIX = 5;
    public static final int SEVEN = 6;
    public static final int SIETE = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final int ELEVEN = 11;
    
    /**
     * path file
     */
    public static final String PATH_IMAGE_NOT_FOUND = "/resources/img/signs-512.png";
    
    
    /**
     * file name and extension
     */
    public static final String IMAGE_PNG = "image/png";
    public static final String IMAGE_NAME_NOT_FOUND = "Sin imagen";
}
